const Discord = require('discord.js');
const { token } = require('./config.json');
const client = new Discord.Client();
const fsLibrary = require('fs');

client.once('ready', () => {
    console.log('it didn\'t immediately die, so that\'s a good sign.');
    client.user.setActivity('myself restart', { type: 'WATCHING' });
});

client.on('message', message => {
    // stop bot responding to self
    if (message.author === client.user) return;

	// if you're immune then you immune
	if (message.member.displayName.toLowerCase() === 'immune') return;

    //if the bot is disabled then it is disabled
    if (message.guild.me.displayName.toLowerCase() === 'disabled') return;
          
    const str = message.content;
    // and, for the lols,
    const strPlus = message.member.displayName + ' ' + message.content;
    
    // say dee a random amount of times (at least once) if dee is said in chat
    if (str === 'dee') {
        if (message.guild.me.displayName.toLowerCase() === 'silent' || message.guild.me.displayName.toLowerCase() === 'silenced') return;
        let count = 0;
        let deeString = '';
        do {
        	deeString += 'dee ';
            //message.channel.send('dee');
            count++;
        } while (Math.random() > 0.5);

        message.channel.send(deeString.substr(0, 2000));

        // oh my god this is awful
        if (count > 1) {
            message.channel.send(count + ' dees for ' + message.member.user.username.toLowerCase());
            fsLibrary.readFile('dee.txt', (error, highestDee) => {
                if (error) throw err;
                if (highestDee < count) {
                    fsLibrary.writeFile('./champion.txt', message.member.user.username.toLowerCase(), (error) => {
                        if (error) throw err;
                    })
                    fsLibrary.writeFile('./dee.txt', count.toString(), (error) => {
                        if (error) throw err;
                    })
                    message.channel.send(message.member.user.username.toLowerCase() + ' is now the dee champion');
                }
            })
        }
    }

    if (str === 'dedede') {
        message.channel.send('dee dee dee');
        message.channel.send(3 + ' dees for ' + message.member.user.username.toLowerCase());
    }

    // probably the most humiliating code i have ever written,
    if (str === 'highest dee'.toLowerCase() || str === 'dee champion'.toLowerCase() || str === 'champion dee'.toLowerCase()|| str === 'deechamp'.toLowerCase()) {
        if (message.guild.me.displayName.toLowerCase() === 'silent' || message.guild.me.displayName.toLowerCase() === 'silenced') return;
        fsLibrary.readFile('dee.txt', (error, highestDee) => {
            if (error) throw err;
            fsLibrary.readFile('champion.txt', (error, championDee) => {
                if (error) throw err;
                message.channel.send(championDee + ', with ' + highestDee + ' dees.');
            })
        })
    }

    let combo = 0;

    if (str === message.member.displayName.toLowerCase() && str !== message.guild.me.nickname.toLowerCase()) {
        if (message.guild.me.displayName.toLowerCase() === 'silent' || message.guild.me.displayName.toLowerCase() === 'silenced' || message.guild.me.displayName.toLowerCase() === 'dee only') return;
        message.channel.send('you');
        combo++;
    }

    if (str === message.guild.me.nickname.toLowerCase() && str !== message.member.displayName.toLowerCase()) {
        if (message.guild.me.displayName.toLowerCase() === 'silent' || message.guild.me.displayName.toLowerCase() === 'silenced' || message.guild.me.displayName.toLowerCase() === 'dee only') return;
        message.channel.send('me');
        combo++;
    }

    if (str === message.member.displayName.toLowerCase() && str === message.guild.me.nickname.toLowerCase()) {
        if (message.guild.me.displayName.toLowerCase() === 'silent' || message.guild.me.displayName.toLowerCase() === 'silenced' || message.guild.me.displayName.toLowerCase() === 'dee only') return;
        message.channel.send('us');
        combo++;
    }

    if (message.member.displayName.toLowerCase() === message.member.user.username.toLowerCase()) {
        if (message.guild.me.displayName.toLowerCase() === 'silent' || message.guild.me.displayName.toLowerCase() === 'silenced' || message.guild.me.displayName.toLowerCase() === 'dee only') return;
        message.channel.send('coward');
        combo++;
    }

    if (strPlus.toLowerCase().includes('owned')) {
        message.channel.send("https://media1.tenor.com/images/747b9875857fd61bf8cdcdd96ef0b472/tenor.gif?itemid=16340765");
        combo++;
    }

    // says the respective string in response to a match of one of the below strings
    switch(str.toLowerCase()) {
        case 'ping':
            if (message.guild.me.displayName.toLowerCase() === 'silent' || message.guild.me.displayName.toLowerCase() === 'silenced' || message.guild.me.displayName.toLowerCase() === 'dee only') return;
            message.channel.send('pong');
            combo++;
            break;
        case 'pong':
            if (message.guild.me.displayName.toLowerCase() === 'silent' || message.guild.me.displayName.toLowerCase() === 'silenced' || message.guild.me.displayName.toLowerCase() === 'dee only') return;
            message.channel.send('ping');
            combo++;
            break;
        case 'ding':
            if (message.guild.me.displayName.toLowerCase() === 'silent' || message.guild.me.displayName.toLowerCase() === 'silenced' || message.guild.me.displayName.toLowerCase() === 'dee only') return;
            message.channel.send('dong');
            combo++;
            break;
        case 'dong':
            if (message.guild.me.displayName.toLowerCase() === 'silent' || message.guild.me.displayName.toLowerCase() === 'silenced' || message.guild.me.displayName.toLowerCase() === 'dee only') return;
            message.channel.send('ding');
            combo++;
            break;
        case 'pingas':
            if (message.guild.me.displayName.toLowerCase() === 'silent' || message.guild.me.displayName.toLowerCase() === 'silenced' || message.guild.me.displayName.toLowerCase() === 'dee only') return;
            message.channel.send('pongas');
            combo++;
            break;
        case 'pongas':
            if (message.guild.me.displayName.toLowerCase() === 'silent' || message.guild.me.displayName.toLowerCase() === 'silenced' || message.guild.me.displayName.toLowerCase() === 'dee only') return;
            message.channel.send('pingas');
            combo++;
            break;
        case 'dingas':
            if (message.guild.me.displayName.toLowerCase() === 'silent' || message.guild.me.displayName.toLowerCase() === 'silenced' || message.guild.me.displayName.toLowerCase() === 'dee only') return;
            message.channel.send('dongas');
            combo++;
            break;
        case 'dongas':
            if (message.guild.me.displayName.toLowerCase() === 'silent' || message.guild.me.displayName.toLowerCase() === 'silenced' || message.guild.me.displayName.toLowerCase() === 'dee only') return;
            message.channel.send('dingas');
            combo++;
            break;
        case 'good morning':
            if (message.guild.me.displayName.toLowerCase() === 'silent' || message.guild.me.displayName.toLowerCase() === 'silenced' || message.guild.me.displayName.toLowerCase() === 'dee only') return;
            // nancy pelosi bot
            message.channel.send('sunday morning');
            combo++;
            break;
        case 'boo':
            if (message.guild.me.displayName.toLowerCase() === 'silent' || message.guild.me.displayName.toLowerCase() === 'silenced' || message.guild.me.displayName.toLowerCase() === 'dee only') return;
            message.channel.send('urns');
            combo++;
            break;
        default:
            break;
    }
    
    // thank you ella for helping me with the section and introducing me to regexes
    // i may have gone a bit overboard with the regexes but who cares about speed amirite?
    const regexIm = /^(.+ )?((i( a|['’ʼ])?m)|(my name( i|['’ʼ])s)) +(.+)/gmi;
    let matchesIm = regexIm.exec(strPlus);
    
    const regexYoure = /^(.+ )?((you(( a|['’ʼ]| sure a)?re|r name( i|['’ʼ])s))|(u( r|(['’ʼ])?re))) +(.+)/gmi;
    let matchesYoure = regexYoure.exec(strPlus);
    
    const regexWere = /^(.+ )?(everyone( is|['’ʼ]?s)|we( are|['’ʼ]re) all) +(.+)/gmi;
    let matchesWere = regexWere.exec(strPlus);
    
    const regexStatus = /^(.+ )?(play|listen to|watch) +(.+)/gmi;
    let matchesStatus = regexStatus.exec(strPlus);
    
    const regexThank = /^(.+ )?(t((h(x|nx|nkx|a(mk|nk|bk|nx|nkx)(s| u|u| you| ya))?)|(y))|((i )?(appreciate (ya|u|you)))) +(.+)/gmi;
    let matchesThank = regexThank.exec(strPlus);
    
    const regexShut = /^(.+ )?((shu(t|p)( the| your| ur)? (up|fuck( up| off)))|kys|(kill|fuck) ((yo)?urself|(yo)?u)|die) +(.+)/gmi;
    let matchesShut = regexShut.exec(strPlus);

    const regexLove = /^(.+ )?((l(uv|ove)|<3) (y)?((o)?u|a)) +(.+)/gmi;
    let matchesLove = regexLove.exec(strPlus);

    const regexHate = /^(.+ )?((hate) (y)?((o)?u|a)) +(.+)/gmi;
    let matchesHate = regexHate.exec(strPlus);
    
    const regexHi = /^(.+ )?((hi(ya)?)|(he(ll|l|nl)o)|(hey(a)?)) +(.+)/gmi;
    let matchesHi = regexHi.exec(strPlus);

    const regexSay = /^(.+ )?(say|repeat|articulate|announce|declare|tell me) +(.+)/gmi;
    let matchesSay = regexSay.exec(strPlus);

    const regexShout = /^(.+ )?(assert|yell|scream|shout|proclaim|shriek) +(.+)/gmi;
    let matchesShout = regexShout.exec(strPlus);

    const regexWhisper = /^(.+ )?(whisper|mumble|mutter|sigh) +(.+)/gmi;
    let matchesWhisper = regexWhisper.exec(strPlus);

    const regexMy = /^(.+ )?(my) +(.+)/gmi;
    let matchesMy = regexMy.exec(str);

    const regexYour = /^(.+ )?((yo)?ur) +(.+)/gmi;
    let matchesYour = regexYour.exec(str);

    // checks for if the regex matches the message sent in channel
    if (matchesIm != null) {
        // if user tries to nickname themselves as their discord username, rightfully call them a coward
        if (matchesIm[7].toLowerCase() === message.member.user.username.toLowerCase()) {
            if (message.guild.me.displayName.toLowerCase() === 'silent' || message.guild.me.displayName.toLowerCase() === 'silenced' || message.guild.me.displayName.toLowerCase() === 'dee only') return;
            message.channel.send('coward');
            combo++;
        // if user tries to change their nickname to their current nickname, tell them you know
        } else if (matchesIm[7].toLowerCase() === message.member.displayName.toLowerCase()) {
            if (message.guild.me.displayName.toLowerCase() === 'silent' || message.guild.me.displayName.toLowerCase() === 'silenced' || message.guild.me.displayName.toLowerCase() === 'dee only') return;
            message.channel.send('i know.');
            combo++;
        } else {
        // returns a cut down (i.e. character limit friendly) version of the new nickname
        message.member.setNickname(matchesIm[7].substr(0, 32));
        }
    } 
    
    if (matchesStatus != null) {
        let statusType;
        let statusSay;
        switch (matchesStatus[2].toLowerCase()) {
            case 'watch':
                statusType = 'WATCHING';
                statusSay = statusType;
                break;
            case 'listen to':
                statusType = 'LISTENING';
                statusSay = statusType + ' to';
                break;
            case 'play':
                statusType = 'PLAYING';
                statusSay = statusType;
                break;
        }
        client.user.setActivity(matchesStatus[3].substr(0, 128), { type : statusType });
        if (message.guild.me.displayName.toLowerCase() === 'silent' || message.guild.me.displayName.toLowerCase() === 'silenced' || message.guild.me.displayName.toLowerCase() === 'dee only') return;
        message.channel.send(statusSay.toLowerCase() + ' ' + matchesStatus[3]);
        combo++;
    }
    
    // you should know the drill by now
    if (matchesYoure != null) {
        if (matchesYoure[10].toLowerCase() === message.guild.me.nickname.toLowerCase()) {
            if (message.guild.me.displayName.toLowerCase() === 'silent' || message.guild.me.displayName.toLowerCase() === 'silenced' || message.guild.me.displayName.toLowerCase() === 'dee only') return;
            message.channel.send('i know.');
            combo++;
        } else if (matchesYoure[10].toLowerCase() === 'pointless') {
            if (message.guild.me.displayName.toLowerCase() === 'silent' || message.guild.me.displayName.toLowerCase() === 'silenced' || message.guild.me.displayName.toLowerCase() === 'dee only') return;
            message.channel.send('39 Buried, 0 Found');
            combo++
        } else {
            message.guild.me.setNickname(matchesYoure[10].substr(0, 32));
            if (message.guild.me.displayName.toLowerCase() === 'silent' || message.guild.me.displayName.toLowerCase() === 'silenced' || message.guild.me.displayName.toLowerCase() === 'dee only') return;
            message.channel.send(`i\'m ${matchesYoure[10]}`);
            combo++;
        }
    }
    
    if (matchesWere != null) {
        message.guild.members.fetch();
        message.guild.members.cache.forEach(r=>r.setNickname(matchesWere[5].substr(0, 32)));
    }
    
    if (matchesThank != null) {
        // using the regex match to check for the bot's nickname doesn't work for some reason ok
        if (matchesThank[13].toLowerCase().includes('my epic bot') || str.toLowerCase().toLowerCase().includes(message.guild.me.nickname) || (/^(.+ )?(zac(h|hary)?(['’ʼ]s|s)?)?( )?bot+(.+)?/gmi).exec(str) != null) { 
            if (message.guild.me.displayName.toLowerCase() === 'silent' || message.guild.me.displayName.toLowerCase() === 'silenced' || message.guild.me.displayName.toLowerCase() === 'dee only') return;
                message.channel.send('you\'re welcome');
                message.member.setNickname('welcome');
                combo++;
            }
    }

    if (matchesHi != null) {
        if (matchesHi[9].toLowerCase().includes('my epic bot') || str.toLowerCase().toLowerCase().includes(message.guild.me.nickname) || (/^(.+ )?(zac(h|hary)?(['’ʼ]s|s)?)?( )?bot+(.+)?/gmi).exec(str) != null) {
            if (message.guild.me.displayName.toLowerCase() === 'silent' || message.guild.me.displayName.toLowerCase() === 'silenced' || message.guild.me.displayName.toLowerCase() === 'dee only') return;
            message.channel.send('hi ' + message.member.displayName);
            combo++;
        }
    }
    
    if (matchesShut != null) {
        if (matchesShut[12].toLowerCase().includes('my epic bot') || str.toLowerCase().toLowerCase().includes(message.guild.me.nickname) || (/^(.+ )?(zac(h|hary)?(['’ʼ]s|s)?)?( )?bot+(.+)?/gmi).exec(str) != null) { 
            if (message.guild.me.displayName.toLowerCase() === 'silent' || message.guild.me.displayName.toLowerCase() === 'silenced' || message.guild.me.displayName.toLowerCase() === 'dee only') return;
                message.channel.send('i\'m just trying my best :cry:');
                message.guild.me.setNickname('just trying my best 😢');
                combo++;
            }
    }

    if (matchesLove != null) {
        if (matchesLove[8].toLowerCase().includes('my epic bot') || str.toLowerCase().toLowerCase().includes(message.guild.me.nickname) || (/^(.+ )?(zac(h|hary)?(['’ʼ]s|s)?)?( )?bot+(.+)?/gmi).exec(str) != null) {
            if (message.guild.me.displayName.toLowerCase() === 'silent' || message.guild.me.displayName.toLowerCase() === 'silenced' || message.guild.me.displayName.toLowerCase() === 'dee only') return;
                message.channel.send('luv u too :heart:');
                combo++;
            }
    }

    if (matchesHate != null) {
        if (matchesHate[7].toLowerCase().includes('my epic bot') || str.toLowerCase().toLowerCase().includes(message.guild.me.nickname) || (/^(.+ )?(zac(h|hary)?(['’ʼ]s|s)?)?( )?bot+(.+)?/gmi).exec(str) != null) {
            if (message.guild.me.displayName.toLowerCase() === 'silent' || message.guild.me.displayName.toLowerCase() === 'silenced' || message.guild.me.displayName.toLowerCase() === 'dee only') return;
                message.channel.send('everyone is so mean 2 me :broken_heart:');
                message.guild.members.cache.forEach(r=>r.setNickname('so mean 2 me 💔'));
                combo++;
            }
    }

    if (matchesSay != null) {
        if (message.guild.me.displayName.toLowerCase() === 'silent' || message.guild.me.displayName.toLowerCase() === 'silenced' || message.guild.me.displayName.toLowerCase() === 'dee only') return;
        message.channel.send(matchesSay[3]);
        combo++;
    }

    if (matchesShout != null) {
        if (message.guild.me.displayName.toLowerCase() === 'silent' || message.guild.me.displayName.toLowerCase() === 'silenced' || message.guild.me.displayName.toLowerCase() === 'dee only') return;
        message.channel.send('**' + matchesShout[3].toUpperCase() + '!!!!!!!!!!!!**');
        combo++;
    }

    if (matchesWhisper != null) {
        if (message.guild.me.displayName.toLowerCase() === 'silent' || message.guild.me.displayName.toLowerCase() === 'silenced' || message.guild.me.displayName.toLowerCase() === 'dee only') return;
        message.channel.send('*' + matchesWhisper[3].toLowerCase() + '...*');
        combo++;
    }

    if (matchesMy != null) {
        if (message.guild.me.displayName.toLowerCase() === 'silent' || message.guild.me.displayName.toLowerCase() === 'silenced' || message.guild.me.displayName.toLowerCase() === 'dee only') return;
        if (matchesMy[1] === undefined) {
            message.channel.send('your ' + matchesMy[3]);
        } else {
            message.channel.send(matchesMy[1] + 'your ' + matchesMy[3]);
        }
        combo++;
    }

    if (matchesYour != null) {
        if (message.guild.me.displayName.toLowerCase() === 'silent' || message.guild.me.displayName.toLowerCase() === 'silenced' || message.guild.me.displayName.toLowerCase() === 'dee only') return;
        if (matchesYour[1] === undefined) {
            message.channel.send('my ' + matchesYour[4]);
        } else {
            message.channel.send(matchesYour[1] + 'my ' + matchesYour[4]);
        }
        combo++;
    }
    
    if (combo > 1) {
        if (message.guild.me.displayName.toLowerCase() === 'silent' || message.guild.me.displayName.toLowerCase() === 'silenced' || message.guild.me.displayName.toLowerCase() === 'dee only') return;
        message.channel.send(combo + 'x combo!');
    }

    
});

client.login(token);
